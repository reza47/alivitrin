$(document).ready(function () {

    function formatState (state) {
      if (!state.id) { return state.text; }
      var $state = $(
        '<span class="label label-' + state.element.value.toLowerCase() + '">' + state.text + '</span>'
      );
      return $state;
    };
    
    //<span class="label label-default">Default</span>

    
    // jquery dropdown
      $(".js-example-basic-hide-search").select2({
          minimumResultsForSearch: Infinity,
          templateResult: formatState
        });

    
    $(".js-example-basic-hide-search").on("change", function (e) { 
        console.log("val: " + $(this).val());
        var $target = $(this).val();
        if ($target != 'all') {
          $('tbody tr').css('display', 'none');
          $('tbody tr[data-status="' + $target + '"]').fadeIn('slow');
        } else {
          $('tbody tr').css('display', 'none').fadeIn('slow');
        }
    });


    $(document).ready(function() {
        // executes when HTML-Document is loaded and DOM is ready
       
       // breakpoint and up  
       $(window).resize(function(){
           if ($(window).width() >= 980){	
       
             // when you hover a toggle show its dropdown menu
             $(".navbar .dropdown-toggle").hover(function () {
                $(this).parent().toggleClass("show");
                $(this).parent().find(".dropdown-menu").toggleClass("show"); 
              });
       
               // hide the menu when the mouse leaves the dropdown
             $( ".navbar .dropdown-menu" ).mouseleave(function() {
               $(this).removeClass("show");  
             });
         
               // do something here
           }	
       });  
         
         
       
       // document ready  
       });